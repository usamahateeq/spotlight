<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $primaryKey = 'like_id';

    public function video()
    {
        return $this->belongsTo('App\Video');
    }
}
