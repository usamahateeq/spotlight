<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventFollows extends Model
{
    protected $table = 'event_follows';

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
