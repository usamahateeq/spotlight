<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $primaryKey = 'event_id';
    //
    public function videos()
    {
        return $this->hasMany('App\Video');
    }

    public function followers()
    {
        return $this->hasMany('App\EventFollows');
    }

}


