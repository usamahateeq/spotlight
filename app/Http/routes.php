<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('events','eventsController');
Route::resource('events.videos','videosController');
Route::resource('events.videos.comments','commentsController');
Route::resource('events.videos.likes','likesController');

Route::post('followEvent','eventsController@follow');
