<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class commentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($event_id,$video_id)
    {
        $comments = Event::find($event_id)->videos->find($video_id)->comments;

        return response()->json(array(
            'error' => false,
            'comments' => $comments
        ));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$event_id,$video_id)
    {
//        $user_id = $request->user_id;
//        $title = $request->title;
//
//        $video = new Video();
//        $video->video_id = Uuid::uuid();
//        $video->event_id = $event_id;
//        $video->user_id = $user_id;
//        $video->title = $title;
//
//        $video ->save();
//
//        return response()->json(
//            array(
//                'error' => false,
//                'video_id' => $video->toArray(),
//                'filename' => $stringName),
//            200
//        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($event_id,$video_id,$comment_id)
    {
        $comment = Event::find($event_id)->videos->find($video_id)->comments->find($comment_id);

        return response()->json(array(
            'error' => false,
            'comments' => $comment
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
