<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventFollows;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class eventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $events = Event::all();
//        $events =   DB::table('events')
//                    ->leftjoin('event_follows','events.event_id','event_follows.event_id')
//                    ->where('event_follows.user_id',$user_id)
//                    ->orderBy('event_follows')
//                    ->get();
        $videos = [];

        foreach($events as $event)
        {
            $eventItem = $event->toArray();
            $videos = $event->videos;
            $eventItem['videos']= $videos;
            $videoLength = 0;

            foreach ($videos as $video)
            {
                $videoItem = $video->toArray();
                $videoItem['comments'] = $video->comments;
                $videoItem['likes'] = $video->likes;
                //$video->video_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$video->video_url;
                $video->video_url = 'http://spotlight-file-bucket.s3-website-us-east-1.amazonaws.com/'.$video->video_url;
                $videoLength  = $videoLength  + (int)$video->video_length;
            }
            $event['total_video_length'] = (string)$videoLength;
        }

        return response()->json(
            array(
                'error' => false,
                'events' => $events->toArray()),
                200
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        $videos = [];

        $eventItem = $event->toArray();
        $videos = $event->videos;
        $eventItem['videos']= $videos;

        $videoLength = 0;

        foreach ($videos as $video)
        {
            $videoItem = $video->toArray();
            $videoItem['comments'] = $video->comments;
            $videoItem['likes'] = $video->likes;
            //$video->video_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$video->video_url;
            $video->video_url = 'http://spotlight-file-bucket.s3-website-us-east-1.amazonaws.com/'.$video->video_url;
            $videoLength  = $videoLength  + (int)$video->video_length;
        }
        $event['total_video_length'] = (string)$videoLength;


        return response()->json(
            array(
                'error' => false,
                'events' => $event->toArray()),
            200
        );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function follow(Request $request)
    {
        $event_id = $request->event_id;
        $user_id = $request->user_id;

        $follow = new EventFollows();
        $follow->event_id = $event_id;
        $follow->user_id = $user_id;
        $follow->save();

        return response()->json(
            array(
                'error' => false,
                'follower_id' => (string)$follow->id),
            200
        );

    }
}
