<?php

namespace App\Http\Controllers;


use App\Event;
use App\Video;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class videosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($event_id)
    {
        //
        $event = Event::find($event_id);
        $videos = $event->videos;

        foreach ($videos as $video)
        {
            $videoItem = $video->toArray();
            $videoItem['comments'] = $video->comments;
            $videoItem['$likes'] = $video->likes;
        }

        return response()->json(
            array(
                'error' => false,
                'videos' => $videos->toArray()),
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$event_id)
    {
        $event_id = $event_id;
        $user_id = $request->user_id;
        $title = $request->title;

//        $file = $request->file('video');
//        $extension = $file->getClientOriginalExtension();
        $stringName = base_path().'\\public\\uploads\\'.$event_id.'.'.str_random(3).'.FLV';
//        Storage::disk('local')->put($stringName,File::get($file));

        $video = new Video();
        $video->video_id = Uuid::uuid();
        $video->event_id = $event_id;
        $video->user_id = $user_id;
        $video->title = $title;

        $video ->save();

        return response()->json(
            array(
                'error' => false,
                'video_id' => $video->toArray(),
                'filename' => $stringName),
            200
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($event_id,$video_id)
    {
        $video = Event::find($event_id)->videos->find($video_id);

        $videoItem = $video->toArray();
        $videoItem['comments'] = $video->comments;
        $videoItem['likes'] = $video->likes;

        return response()->json(
            array(
                'error' => false,
                'videos' => $video->toArray()),
            200
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
