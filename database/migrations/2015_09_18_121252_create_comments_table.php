<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->string('comment_id')->primary();
            $table->string('video_id');
            $table->string('user_id');
            $table->integer('offset_in_video');
            $table->timestamp('uploadTimestamp');
            $table->string('comment_text',255);
            $table->timestamps();

            $table->foreign('video_id')->references('video_id')->on('videos');
            $table->foreign('user_id')->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
