<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->string('video_id')->primary();
            $table->string('event_id');
            $table->string('title');
            $table->timestamp('uploadTimestamp');
            $table->string('image_url');
            $table->string('video_url');
            $table->string('user_id');
            $table->timestamps();

            $table->foreign('event_id')->references('event_id')->on('events');
            $table->foreign('user_id')->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
