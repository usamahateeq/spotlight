<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEventFollowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('event_follows', function (Blueprint $table) {
            $table->increments('follower_id');
            $table->string('event_id');
            $table->string('user_id');
            $table->timestamps();

            $table->foreign('event_id')->references('event_id')->on('events');
            $table->foreign('user_id')->references('user_id')->on('users');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_follows');
    }
}
