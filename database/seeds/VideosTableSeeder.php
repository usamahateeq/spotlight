<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
            'video_id' => 'video1',
            'event_id' => 'event1',
            'title' => 'VIDEO1-'.str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
            'video_url' => 'http://www.dailymotion.com/video/x15tr95_funny-guilty-dog-walking-silently-down-the-house-lol_animals',
        ]);

        DB::table('videos')->insert([
            'video_id' => 'video2',
            'event_id' => 'event1',
            'title' => 'VIDEO2-'.str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
            'video_url' => 'http://www.dailymotion.com/video/x16ovz1_bcnarenas',
        ]);
        DB::table('videos')->insert([
            'video_id' => 'video3',
            'event_id' => 'event1',
            'title' => 'VIDEO3-'.str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
            'video_url' => 'http://www.dailymotion.com/video/x37dmel_gta-5-actor-recruitment-ttu-series-music-videos-cinematic-short-films_tv',
        ]);
        DB::table('videos')->insert([
            'video_id' => 'video4',
            'event_id' => 'event1',
            'title' => 'VIDEO4-'.str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
            'video_url' => 'http://www.dailymotion.com/video/x2ivmdd_tribeca-short-films-from-olivia-wilde-katie-holmes-added-to-lineup_people',
        ]);
        DB::table('videos')->insert([
            'video_id' => 'video5',
            'event_id' => 'event1',
            'title' => 'VIDEO5-'.str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
            'video_url' => 'http://www.dailymotion.com/video/x16owb5_mvi-0239_lifestyle',
        ]);

        DB::table('videos')->insert([
            'video_id' => 'video1OfEvent2',
            'event_id' => 'event2',
            'title' => 'VIDEO1Event2-'.str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
            'video_url' => 'http://www.dailymotion.com/video/x14bt7m_guilty-dog_fun',
        ]);

        DB::table('videos')->insert([
            'video_id' => 'video2OfEvent2',
            'event_id' => 'event2',
            'title' => 'VIDEO2Event2-'.str_random(10),
            'user_id' => 'user2',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
            'video_url' => 'http://www.dailymotion.com/video/x2ubxzo_guilty-dog-slips-through-door-after-getting-caught_fun',
        ]);

        DB::table('videos')->insert([
            'video_id' => 'video1OfEvent3',
            'event_id' => 'event3',
            'title' => 'VIDEO1Event3-'.str_random(10),
            'user_id' => 'user2',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
            'video_url' => 'http://www.dailymotion.com/video/x2ubxzo_guilty-dog-slips-through-door-after-getting-caught_fun',
        ]);
    }
}
