<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'event_id' => 'event1',
            'title' => 'EVENT1-'.str_random(10),
            'geo' => str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'https://www.zenimax.com/jpn/fallout3/images/avators/100x100falloutav-vb.gif',
        ]);

        DB::table('events')->insert([
            'event_id' => 'event2',
            'title' => 'EVENT2-'.str_random(10),
            'geo' => str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'http://picture-cdn.wheretoget.it/vhclr2-i.jpg',
        ]);

        DB::table('events')->insert([
            'event_id' => 'event3',
            'title' => 'EVENT3-'.str_random(10),
            'geo' => str_random(10),
            'user_id' => 'user2',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'http://images.forbes.com/media/lists/companies/apple_100x100.jpg',
        ]);

        DB::table('events')->insert([
            'event_id' => 'event4',
            'title' => 'EVENT4-'.str_random(10),
            'geo' => str_random(10),
            'user_id' => 'user2',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'http://images.forbes.com/media/lists/companies/apple_100x100.jpg',
        ]);

        DB::table('events')->insert([
            'event_id' => 'event5',
            'title' => 'EVENT5-'.str_random(10),
            'geo' => str_random(10),
            'user_id' => 'user1',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'http://images.forbes.com/media/lists/companies/apple_100x100.jpg',
        ]);

        DB::table('events')->insert([
            'event_id' => 'event6',
            'title' => 'EVENT6-'.str_random(10),
            'geo' => str_random(10),
            'user_id' => 'user2',
            'uploadTimestamp' => date_timestamp_get(date_create()),
            'image_url' => 'http://images.forbes.com/media/lists/companies/apple_100x100.jpg',
        ]);


    }
}
