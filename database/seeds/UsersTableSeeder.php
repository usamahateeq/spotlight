<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'user_id' => 'user1',
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->insert([
            'user_id' => 'user2',
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->insert([
            'user_id' => 'user3',
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->insert([
            'user_id' => 'user4',
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'user_id' => 'user5',
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->insert([
            'user_id' => 'user6',
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
