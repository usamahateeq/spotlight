<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'comment_id' => 'C1V1E1',
            'video_id' => 'video1',
            'user_id' => 'user2',
            'offset_in_video' => 10,
            'comment_text' => 'This video is cool',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('comments')->insert([
            'comment_id' => 'C2V1E1',
            'video_id' => 'video1',
            'user_id' => 'user1',
            'offset_in_video' => 12,
            'comment_text' => 'Awesome!',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('comments')->insert([
            'comment_id' => 'C3V1E1',
            'video_id' => 'video1',
            'user_id' => 'user2',
            'offset_in_video' => 14,
            'comment_text' => 'I like the video',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('comments')->insert([
            'comment_id' => 'C4V1E1',
            'video_id' => 'video1',
            'user_id' => 'user1',
            'offset_in_video' => 16,
            'comment_text' => 'Can you upload more ?',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('comments')->insert([
            'comment_id' => 'C5V1E1',
            'video_id' => 'video1',
            'user_id' => 'user2',
            'offset_in_video' => 17,
            'comment_text' => 'Great and funny ',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('comments')->insert([
            'comment_id' => 'C1V2E1',
            'video_id' => 'video2',
            'user_id' => 'user1',
            'offset_in_video' => 18,
            'comment_text' => 'thumbs up',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('comments')->insert([
            'comment_id' => 'C2V2E1',
            'video_id' => 'video2',
            'user_id' => 'user2',
            'offset_in_video' => 20,
            'comment_text' => 'boring',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('comments')->insert([
            'comment_id' => 'C3V2E1',
            'video_id' => 'video2',
            'user_id' => 'user1',
            'offset_in_video' => 15,
            'comment_text' => 'so funny',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('comments')->insert([
            'comment_id' => 'C4V2E1',
            'video_id' => 'video2',
            'user_id' => 'user2',
            'offset_in_video' => 16,
            'comment_text' => '5 stars',
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);
    }
}
