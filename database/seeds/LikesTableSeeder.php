<?php

use Illuminate\Database\Seeder;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('likes')->insert([
            'like_id' => 'L1V1E1',
            'video_id' => 'video1',
            'user_id' => 'user2',
            'offset_in_video' => 10,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('likes')->insert([
            'like_id' => 'L2V1E1',
            'video_id' => 'video1',
            'user_id' => 'user1',
            'offset_in_video' => 12,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('likes')->insert([
            'like_id' => 'L3V1E1',
            'video_id' => 'video1',
            'user_id' => 'user2',
            'offset_in_video' => 14,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('likes')->insert([
            'like_id' => 'L4V1E1',
            'video_id' => 'video1',
            'user_id' => 'user1',
            'offset_in_video' => 16,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('likes')->insert([
            'like_id' => 'L5V1E1',
            'video_id' => 'video1',
            'user_id' => 'user2',
            'offset_in_video' => 17,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('likes')->insert([
            'like_id' => 'L1V2E1',
            'video_id' => 'video2',
            'user_id' => 'user1',
            'offset_in_video' => 18,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('likes')->insert([
            'like_id' => 'L2V2E1',
            'video_id' => 'video2',
            'user_id' => 'user2',
            'offset_in_video' => 20,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('likes')->insert([
            'like_id' => 'L3V2E1',
            'video_id' => 'video2',
            'user_id' => 'user1',
            'offset_in_video' => 15,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);

        DB::table('likes')->insert([
            'like_id' => 'L4V2E1',
            'video_id' => 'video2',
            'user_id' => 'user2',
            'offset_in_video' => 16,
            'uploadTimestamp' => date_timestamp_get(date_create()),
        ]);
    }
}
